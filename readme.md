# better coding with vscode

## navigate to methods and functions

open command pallet and type @
or ctrl + shift + .

## find some method or function in entire project

open command pallet and use #
you can use first letter of camel case to find anything

## go to line number

hit ctrl + g to go to line number

## modify a word in entire file

use ctrl + d to modify all same word in entire file

## highlight lines

use ctrl + l

## terminal

use ctrl + arrows to move along words

## tasks

task: configure default build task
run from command pallet

## snippets

use snippets for projects

## my extensions

* auto rename tag
* better commands
* code spell checker
* eslint
* html css support
* live server
* markdownlint
* material icon
* php debug
* php intelephense
* prettier
* quokka
* reactjs code snippets
* regex previewer
* vetur
* volar
* add jsdoc comments
* gitlens
* remote repositories

## my themes

* synthwave 84
* synthwave 84 neon
